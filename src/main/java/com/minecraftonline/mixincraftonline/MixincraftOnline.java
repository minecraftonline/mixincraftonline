package com.minecraftonline.mixincraftonline;

import com.flowpowered.math.vector.Vector3i;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.google.inject.Inject;
import com.minecraftonline.mixincraftonline.bridge.HopperBridge;
import com.minecraftonline.mixincraftonline.util.InventoryCache;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityHopper;
import org.slf4j.Logger;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.game.state.GameStartingServerEvent;
import org.spongepowered.api.event.world.chunk.UnloadChunkEvent;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.world.World;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Plugin(id = "mixincraftonline", description = "Optimization mixins for MinecraftOnline")
public class MixincraftOnline {

    public static boolean HOPPER_OPTIMISATION_APPLIED = false;

    @Inject
    private Logger logger;

    private static MixincraftOnline INSTANCE;

    // TODO: make this a config value?
    // this world does not tick most things
    public static final String FROZEN_WORLD_NAME = "Lightonia";

    public MixincraftOnline() {
        INSTANCE = this;
    }

    public static MixincraftOnline getInstance() {
        return INSTANCE;
    }

    private Map<UUID, Multimap<Vector3i, InventoryCache>> toInvalidateHopperMap = new HashMap<>();

    public void addInvalidateMap(InventoryCache cache) {
        final Vector3i pos = new Vector3i(cache.getPos().getX(), cache.getPos().getY(), cache.getPos().getZ());
        Vector3i chunkPos = Sponge.getServer().getChunkLayout().forceToChunk(pos);
        final UUID uuid = ((World) cache.getWorld()).getUniqueId();
        toInvalidateHopperMap.compute(uuid, (k,v) -> {
            if (v == null) {
                v = HashMultimap.create();
            }
            v.put(chunkPos, cache);
            return v;
        });
    }

    @Listener
    public void onServerStart(GameStartingServerEvent event) {
        HOPPER_OPTIMISATION_APPLIED = new TileEntityHopper() instanceof HopperBridge;

        if (HOPPER_OPTIMISATION_APPLIED) {
            logger.info("Hopper optimisation successfully applied.");
        }
        else {
            logger.warn("Hopper optimisation not applied.");
        }
    }

    @Listener
    public void onChunkUnload(UnloadChunkEvent event) {
        final UUID uuid = event.getTargetChunk().getWorld().getUniqueId();
        final Vector3i chunkPos = event.getTargetChunk().getPosition();
        Multimap<Vector3i, InventoryCache> map = this.toInvalidateHopperMap.get(uuid);
        if (map != null) {
            for (InventoryCache cache : map.removeAll(chunkPos)) {
                cache.invalidate();
            }
        }
    }
}
