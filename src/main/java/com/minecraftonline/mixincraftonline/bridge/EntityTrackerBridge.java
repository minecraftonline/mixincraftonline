package com.minecraftonline.mixincraftonline.bridge;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.world.chunk.Chunk;

public interface EntityTrackerBridge {
    /**
     * Batch send leashed/passengers to the following players.
     *
     * @param players Players to send to.
     * @param chunk Chunk with the leashed/passenger entities
     */
    void sendLeashedEntitiesInChunkToAll(Iterable<EntityPlayerMP> players, Chunk chunk);
}
