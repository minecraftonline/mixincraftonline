package com.minecraftonline.mixincraftonline.mixins;

import net.minecraft.network.NetHandlerPlayServer;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.SPacketPlayerListItem;
import net.minecraft.server.management.PlayerList;
import net.minecraft.world.GameType;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;

import java.util.List;
import java.util.stream.Collectors;

@Mixin(PlayerList.class)
public class AlwaysTabInSurvivalLoginMixin {

    @Redirect(method = "playerLoggedIn",
            at = @At(value = "INVOKE",
                    target = "Lnet/minecraft/network/NetHandlerPlayServer;sendPacket(Lnet/minecraft/network/Packet;)V"))
    public void onLoginRedirectGamemodeInformingPackets(NetHandlerPlayServer instance, Packet<?> packet) {
        if (packet instanceof SPacketPlayerListItemAccessor) {
            System.out.println("Intercepting gamemode packet.");
            SPacketPlayerListItem packetPlayerListItem = (SPacketPlayerListItem) packet;
            List<SPacketPlayerListItem.AddPlayerData> list = ((SPacketPlayerListItemAccessor) packet).getPlayers();
            List<SPacketPlayerListItem.AddPlayerData> newList = list.stream().map(item -> {
                System.out.println("Modifying packet.");
                if (item.getProfile().getId().equals(instance.player.getUniqueID())) {
                    return item; // Don't blank yourself
                }

                // Wash away all the gamemodes for other players.
                // Its an inner (non-static) class, i literally never use this; what is this syntax.
                return packetPlayerListItem.new AddPlayerData(item.getProfile(), item.getPing(), GameType.SURVIVAL, item.getDisplayName());
            }).collect(Collectors.toList());
            list.clear();
            list.addAll(newList);
        }
    }
}
