package com.minecraftonline.mixincraftonline.mixins;

import net.minecraft.block.Block;
import net.minecraft.block.BlockSnowBlock;
import net.minecraft.block.material.Material;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(BlockSnowBlock.class)
public abstract class BlockSnowBlockMixin extends Block {

    protected BlockSnowBlockMixin(Material materialIn) {
        super(materialIn);
    }

    @Inject(at = @At("TAIL"), method = "<init>")
    public void init(CallbackInfo ci) {
        setTickRandomly(false); // MC-88097 - snow block does not need random ticks
    }
}
