package com.minecraftonline.mixincraftonline.mixins;

import com.minecraftonline.mixincraftonline.bridge.EmptyOrFullInventoryCached;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntityChest;
import net.minecraft.tileentity.TileEntityLockableLoot;
import net.minecraft.util.NonNullList;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(TileEntityChest.class)
public abstract class ChestCacheEmptyFullStateMixin extends TileEntityLockableLoot implements EmptyOrFullInventoryCached {

    @Shadow private NonNullList<ItemStack> chestContents;
    protected Boolean isEmptyCache = null;
    protected Boolean isFullCache = null;

    /**
     * Cache empty state to improve hopper performance.
     * @return Whether its empty
     * @author tyhdefu
     */
    @Overwrite
    public boolean isEmpty() {
        if (this.isEmptyCache == null) {
            this.isEmptyCache = impl$isActuallyEmpty();
        }
        return this.isEmptyCache;
    }

    private boolean impl$isActuallyEmpty() {
        for(ItemStack itemstack : this.chestContents) {
            if (!itemstack.isEmpty()) {
                return false;
            }
        }
        return true;
    }

    @Override
    public void bridge$setEmptyCache(Boolean emptyCache) {
        this.isEmptyCache = emptyCache;
    }

    @Override
    public boolean bridge$isFullUsingCache() {
        if (this.isFullCache == null) {
            this.isFullCache = impl$isActuallyFull();
        }
        return this.isFullCache;
    }

    private boolean impl$isActuallyFull() {
        for(ItemStack itemstack : this.chestContents) {
            if (itemstack.getItem() == Items.AIR || itemstack.getCount() != itemstack.getMaxStackSize()) {
                return false;
            }
        }
        return true;
    }

    @Override
    public void markDirty() {
        super.markDirty();

        this.isFullCache = null;
        this.isEmptyCache = null;
    }
}
