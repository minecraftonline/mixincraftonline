package com.minecraftonline.mixincraftonline.mixins;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.ContainerEnchantment;
import net.minecraft.inventory.Slot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import org.spongepowered.asm.mixin.Debug;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.Redirect;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

@Mixin(ContainerEnchantment.class)
public abstract class ContainerEnchantmentShiftClickFix {

    private ItemStack mxncft$stack = null;

    @Inject(method = "transferStackInSlot", at = @At(value = "NEW", target = "(Lnet/minecraft/item/Item;II)Lnet/minecraft/item/ItemStack;"),
            locals = LocalCapture.CAPTURE_FAILHARD)
    public void storeValues(EntityPlayer playerIn, int index, CallbackInfoReturnable<ItemStack> cir,
                            ItemStack empty, Slot slot, ItemStack stack) {
        this.mxncft$stack = stack;
    }

    @Redirect(method = "transferStackInSlot", at = @At(value = "NEW", target = "(Lnet/minecraft/item/Item;II)Lnet/minecraft/item/ItemStack;"))
    public ItemStack executeSmartCode(Item itemIn, int amount, int meta) {
        ItemStack itemStack = this.mxncft$stack.copy();
        itemStack.setCount(1);
        return itemStack;
    }
}
