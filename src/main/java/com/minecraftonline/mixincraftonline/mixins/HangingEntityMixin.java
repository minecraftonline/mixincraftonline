package com.minecraftonline.mixincraftonline.mixins;

import java.util.List;

import javax.annotation.Nullable;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.Redirect;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import com.google.common.collect.Lists;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityHanging;
import net.minecraft.entity.MoverType;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.world.World;

@Mixin(EntityHanging.class)
public abstract class HangingEntityMixin {

    // we do not want any hanging entities to be destroyed by other entities such as boats or shulkers (mob) colliding with them.
    // other hanging entities will still destroy others as this is checked later in this onValidSurface method.
    // and we still want blocks placed where the item frame is to break it.
    @Redirect(method = "onValidSurface", at = @At(value = "INVOKE", target = "Lnet/minecraft/world/World;getCollisionBoxes(Lnet/minecraft/entity/Entity;Lnet/minecraft/util/math/AxisAlignedBB;)Ljava/util/List;"))
    public List<AxisAlignedBB> onGetCollisionBoxes(World world, @Nullable Entity entityIn, AxisAlignedBB aabb)
    {
        List<AxisAlignedBB> list = Lists.<AxisAlignedBB>newArrayList();
        // gets collision boxes of blocks only, not other entities
        ((WorldAccessor) world).getCollisionBoxes(entityIn, aabb, false, list);
        return list;
    }

    // methods below seem to fix item frames being deleted instead of dropped when pushed by piston or shulker.
    // they'll still break eventually because of the method above finding a block in the same spot though.
    // same goes for if a fence is placed below an item frame.

    @Inject(method = "move", at = @At("HEAD"), cancellable = true)
    public void onMove(MoverType type, double x, double y, double z, CallbackInfo ci) {
        ci.cancel();
    }

    @Inject(method = "addVelocity", at = @At("HEAD"), cancellable = true)
    public void onAddVelocity(double x, double y, double z, CallbackInfo ci) {
         ci.cancel();
    }

}
