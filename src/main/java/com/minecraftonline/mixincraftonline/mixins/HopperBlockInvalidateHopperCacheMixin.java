package com.minecraftonline.mixincraftonline.mixins;

import com.minecraftonline.mixincraftonline.MixincraftOnline;
import com.minecraftonline.mixincraftonline.bridge.HopperBridge;
import net.minecraft.block.Block;
import net.minecraft.block.BlockHopper;
import net.minecraft.block.state.IBlockState;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(BlockHopper.class)
public abstract class HopperBlockInvalidateHopperCacheMixin {

    @Shadow public static EnumFacing getFacing(int meta) { throw new AbstractMethodError("Shadow"); }

    @Shadow public abstract int getMetaFromState(IBlockState state);

    @Inject(method = "neighborChanged", at = @At("RETURN"))
    public void onNeighbourChanged(IBlockState state, World worldIn, BlockPos pos, Block blockIn, BlockPos fromPos, CallbackInfo ci) {
        if (!MixincraftOnline.HOPPER_OPTIMISATION_APPLIED) {
            return; // Not interested.
        }
        if (fromPos.equals(pos.up())) {
            TileEntity tileEntity = worldIn.getTileEntity(pos);
            if (tileEntity instanceof HopperBridge) {
                ((HopperBridge) tileEntity).bridge$invalidateSourceInv();
            }
        }
        else if (pos.offset(getFacing(getMetaFromState(state))).equals(fromPos)) {
            TileEntity tileEntity = worldIn.getTileEntity(pos);
            if (tileEntity instanceof HopperBridge) {
                ((HopperBridge) tileEntity).bridge$invalidateDestInv();
            }
        }
    }
}
