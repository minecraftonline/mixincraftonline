package com.minecraftonline.mixincraftonline.mixins;

import com.minecraftonline.mixincraftonline.MixincraftOnline;

import net.minecraft.entity.Entity;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(net.minecraft.item.ItemMap.class)
public abstract class MapItemMixin {

    @Inject(method = "onUpdate", at = @At("HEAD"), cancellable = true)
    public void onOnUpdate(ItemStack stack, World worldIn, Entity entityIn, int itemSlot, boolean isSelected, CallbackInfo ci) {
        if (worldIn.getWorldInfo().getWorldName().equals(MixincraftOnline.FROZEN_WORLD_NAME)) {
            ci.cancel();
        }
    }
}
