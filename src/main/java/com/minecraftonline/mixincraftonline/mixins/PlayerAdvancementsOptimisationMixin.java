package com.minecraftonline.mixincraftonline.mixins;

import net.minecraft.advancements.Advancement;
import net.minecraft.advancements.AdvancementProgress;
import net.minecraft.advancements.PlayerAdvancements;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.Shadow;

import javax.annotation.Nullable;
import java.util.Map;
import java.util.Set;

@Mixin(PlayerAdvancements.class)
public abstract class PlayerAdvancementsOptimisationMixin {
    @Shadow protected abstract boolean shouldBeVisible(Advancement p_192738_1_);
    @Shadow @Final private Set<Advancement> visible;
    @Shadow @Final private Set<Advancement> visibilityChanged;
    @Shadow @Final private Map<Advancement, AdvancementProgress> progress;
    @Shadow @Final private Set<Advancement> progressChanged;

    /**
     * The original implementation goes recurses checking the parent each time the current has changed,
     * however it also checks all children. I can only imagine the amount of calls this produces,
     * and it's probably well beyond exponential.
     *
     * @param advancement Advancement to ensure visibility for.
     * @reason Because minecraft implementation is shite. It goes up and down
     *          and I think its lucky that it ever terminates.
     * @author tyhdefu
     */
    @Overwrite
    private void ensureVisibility(Advancement advancement) {
        //System.out.println("-- ENSURING VISIBILITY OF ADVANCEMENTS --");
        ensureVisibilitySkippingChild(advancement, null/*, 0*/);
    }

    private void ensureVisibilitySkippingChild(Advancement advancement, @Nullable Advancement skip/*, int depth*/) {
        //System.out.println("Traversing: " + advancement.getId().toString() + " (depth: " + depth + ")");
        boolean shouldBeVisible = this.shouldBeVisible(advancement);
        boolean isVisible = this.visible.contains(advancement);
        if (shouldBeVisible && !isVisible) {
            this.visible.add(advancement);
            this.visibilityChanged.add(advancement);
            if (this.progress.containsKey(advancement)) {
                this.progressChanged.add(advancement);
            }
        } else if (!shouldBeVisible && isVisible) {
            this.visible.remove(advancement);
            this.visibilityChanged.add(advancement);
        }

        if (shouldBeVisible != isVisible && advancement.getParent() != null) {
            if (advancement.getParent() != skip) {
                this.ensureVisibilitySkippingChild(advancement.getParent(), advancement/*, depth + 1*/);
            }
            else {
                //System.out.println("Skipping parent.");
            }
        }

        for(Advancement child : advancement.getChildren()) {
            if (child == skip) {
                //System.out.println("Skipping child.");
                continue;
            }
            this.ensureVisibilitySkippingChild(child, advancement/*, depth + 1*/);
        }
    }
}
