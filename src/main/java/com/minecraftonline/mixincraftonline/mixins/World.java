package com.minecraftonline.mixincraftonline.mixins;

import com.minecraftonline.mixincraftonline.util.SetList;
import net.minecraft.profiler.Profiler;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.WorldProvider;
import net.minecraft.world.storage.ISaveHandler;
import net.minecraft.world.storage.WorldInfo;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Mutable;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.Collections;
import java.util.IdentityHashMap;
import java.util.List;

@Mixin(net.minecraft.world.World.class)
public abstract class World {
    @Mutable
    @Shadow @Final private List<TileEntity> tileEntitiesToBeRemoved;

    @Inject(method = "<init>", at = @At("RETURN"))
    public void onInit(ISaveHandler saveHandlerIn, WorldInfo info, WorldProvider providerIn, Profiler profilerIn, boolean client, CallbackInfo ci) {
        tileEntitiesToBeRemoved = new SetList<>(Collections.newSetFromMap(new IdentityHashMap<>()));
    }
}
