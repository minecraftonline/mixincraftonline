package com.minecraftonline.mixincraftonline.mixins;

import java.util.List;

import javax.annotation.Nullable;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Invoker;

import net.minecraft.entity.Entity;
import net.minecraft.util.math.AxisAlignedBB;

@Mixin(net.minecraft.world.World.class)
public interface WorldAccessor {

    @Invoker("getCollisionBoxes")
    public boolean getCollisionBoxes(@Nullable Entity entityIn, AxisAlignedBB aabb, boolean p_191504_3_, @Nullable List<AxisAlignedBB> outList);

}
