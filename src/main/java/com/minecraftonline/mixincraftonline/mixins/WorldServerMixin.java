package com.minecraftonline.mixincraftonline.mixins;

import java.util.Iterator;

import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import com.minecraftonline.mixincraftonline.MixincraftOnline;

import net.minecraft.profiler.Profiler;
import net.minecraft.server.management.PlayerChunkMap;
import net.minecraft.world.WorldProvider;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.storage.ISaveHandler;
import net.minecraft.world.storage.WorldInfo;

@Mixin(net.minecraft.world.WorldServer.class)
public abstract class WorldServerMixin extends net.minecraft.world.World {
    @Shadow @Final private PlayerChunkMap playerChunkMap;

    protected WorldServerMixin(ISaveHandler saveHandlerIn, WorldInfo info, WorldProvider providerIn,
            Profiler profilerIn, boolean client) {
        super(saveHandlerIn, info, providerIn, profilerIn, client);
    }

    @Inject(method = "tick", at = @At("HEAD"), cancellable = true)
    public void onTick(CallbackInfo ci) {
        if (this.worldInfo.getWorldName().equals(MixincraftOnline.FROZEN_WORLD_NAME)) {
            // only tick a limited amount of things
            this.profiler.endStartSection("chunkSource");
            this.chunkProvider.tick();
            this.profiler.endStartSection("tickBlocks");
            Iterator<Chunk> iterator1 = this.playerChunkMap.getChunkIterator();
            while(iterator1.hasNext()) {
               ((Chunk)iterator1.next()).onTick(false);
            }
            this.profiler.endStartSection("chunkMap");
            this.playerChunkMap.tick();
            this.profiler.endSection();
            ci.cancel();
        }
    }

    @Inject(method = "updateEntities", at = @At("HEAD"), cancellable = true)
    public void onUpdateEntities(CallbackInfo ci) {
        if (this.worldInfo.getWorldName().equals(MixincraftOnline.FROZEN_WORLD_NAME)) {
            ci.cancel();
        }
    }
}
