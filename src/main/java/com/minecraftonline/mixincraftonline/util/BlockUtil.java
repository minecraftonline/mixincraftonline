package com.minecraftonline.mixincraftonline.util;

import net.minecraft.util.math.BlockPos;

public class BlockUtil {

    public static boolean isOnChunkBorder(BlockPos pos) {
        final int xPos = Math.floorMod(pos.getX(), 16);
        final int zPos = Math.floorMod(pos.getZ(), 16);
        return xPos == 0 || xPos == 15 || zPos == 0 || zPos == 15;
    }
}
